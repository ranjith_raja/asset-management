import { LightningElement,api } from 'lwc';

export default class InventoryViewLWC extends LightningElement {
  @api objectApiName;
  @api recordId;
}