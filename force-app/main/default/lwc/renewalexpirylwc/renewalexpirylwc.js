import { LightningElement, api, track, wire } from 'lwc';
import { getRecord } from 'lightning/uiRecordApi';
import getrenewallst from '@salesforce/apex/AssetController.getexppirylst';
import getchildasset from '@salesforce/apex/AssetController.getchidassest';
import formatasset from '@salesforce/apex/InventoryHandler.assetDeactivate';
import EmailEncodingKey from '@salesforce/schema/User.EmailEncodingKey';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';

const FIELDS = ['Inventory__c.Renewal_Warranty_expirydate__c', 'Inventory__c.Warranty_ExpiryRenewal_date__c'];
//const _MS_PER_DAY = 1000 * 60 * 60 * 24;

export default class Renewalexpirylwc extends LightningElement {

    @api recordId;  
    inveny;  
    date;
    @track days;
    @track style='';
    @track renelst;
    error;
    isclose=false;
    @track _renedate;
    @track modalval=false;
    @track childlst;
    @track animeclass='slds-trial-header slds-grid slds-clearfix';
    @track isModalOpen1=false;
   @api
    get renedate() {
        return this._renedate;
    }
    set renedate(value) {
        this.setAttribute('renedate', value);
        this._renedate = value;
        if (this._renedate != null) {
             
            var date1 = new Date(this._renedate);
            var date2 =  new Date();
            date1.setHours(0);
             date1.setMinutes(0, 0, 0);
            date2.setHours(0);
            date2.setMinutes(0, 0, 0);
            var datediff = date1.getTime() - date2.getTime(); // difference 
            var Difference_In_Days=parseInt(datediff / (24 * 60 * 60 * 1000), 10); //Convert values days and return value  
             //var datediff =  new Date(date1.getTime()-date2.getTime()) ;
            // var Difference_In_Days =  (datediff.getUTCDate() - 1) //Math.round(datediff / (1000 * 3600 * 24));
             this.days = Difference_In_Days;
             if(date1<date2)
             {
                this.style='background:#da0606;'; 
                this.animeclass='slds-trial-header slds-grid slds-clearfix';
             }
             else if(Difference_In_Days == 1 || Difference_In_Days == 0)
             {
                this.style='background:#b10404;';   
                this.animeclass +=' anime' ;
             }
             else
             {
                this.style='';  
             }

           }
           else
           {
            this.days = '';
           }
           

        }
    
    
    @wire(getRecord, { recordId: '$recordId', fields: FIELDS })
    wiredRecord({ error, data }) {
        if (error) {

            alert('error');
        }
        else if (data) {
            this.inveny=data;
           this.date= this.inveny.fields.Renewal_Warranty_expirydate__c.value;
           console.log('rrrr' + this.date);
           if( this.date!=null || this.date!=undefined)
           { //alert(this.date);
           
                // Discard the time and time-zone information.
                
              
            var date1 = new Date(this.date);
            var date2 =  new Date();
            date1.setHours(0);
             date1.setMinutes(0, 0, 0);
            date2.setHours(0);
            date2.setMinutes(0, 0, 0);
            var datediff = date1.getTime() - date2.getTime(); // difference 
            var Difference_In_Days=parseInt(datediff / (24 * 60 * 60 * 1000), 10); //Convert values days and return value  
             //var datediff =  new Date(date1.getTime()-date2.getTime()) ;
            // var Difference_In_Days =  (datediff.getUTCDate() - 1) //Math.round(datediff / (1000 * 3600 * 24));
             this.days = Difference_In_Days;
             if(date1<date2)
             {
                this.style='background:#da0606;'; 
                this.animeclass='slds-trial-header slds-grid slds-clearfix';
            
             }
             else if(Difference_In_Days == 1 || Difference_In_Days == 0)
             {
                this.style='background:#b10404;';   
                this.animeclass +=' anime' ;
             }
             else{
                this.style='';  
                this.animeclass='slds-trial-header slds-grid slds-clearfix';
             }

           }
           else
           {
            this.days = '';
           }
          


        }

    }
    @wire(getrenewallst) 
    wiredRecord1({ error, data })
    {
        if (error) {

            alert('error');
        }
        else if (data) {
            let rows = JSON.parse( JSON.stringify( data ) );
            console.log( 'Rows are ' + JSON.stringify( rows ) );
                
            for ( let i = 0; i < rows.length; i++ ) {  

                let dataParse = rows[ i ];
               // dataParse.OwnerName = dataParse.Owner.Name;
                dataParse.OwnerURL = "/" + dataParse.Id;
                var date1 = new Date(dataParse.Renewal_Warranty_expirydate__c);
                 var date2 =  new Date();
                date1.setHours(0);
                date1.setMinutes(0, 0, 0);
                date2.setHours(0);
                date2.setMinutes(0, 0, 0);
            var datediff = date1.getTime() - date2.getTime(); // difference 
            var Difference_In_Days=parseInt(datediff / (24 * 60 * 60 * 1000), 10);
            dataParse.days=Difference_In_Days;
            if(Difference_In_Days >=0 || Difference_In_Days<=2)
            {
                dataParse.h2sty='color: red;';
            }
            else if(Difference_In_Days >2 || Difference_In_Days<=5)
            {
                dataParse.h2sty='color: green;';
            }
            else
            {
                dataParse.h2sty='color: black;';
            }
            
            }
                
            this.renelst = rows;
            this.error = undefined;

        }


    }

@wire(getchildasset,{invid:'$recordId'})
wiredRecord3({ error, data })
{
    if (error) {

        alert('error');
    }
    else if (data) {
        let rows = JSON.parse( JSON.stringify( data ) );
        console.log( 'Rows are ' + JSON.stringify( rows ) );
            
        for ( let i = 0; i < rows.length; i++ ) {  

            let dataParse = rows[ i ];
           // dataParse.OwnerName = dataParse.Owner.Name;
            dataParse.OwnerURL = "/" + dataParse.Id;
            
        this.childlst = rows;
        this.error = undefined;

    }
}
}
handlechilddelte()
{
    let invid=[];
    invid.push(this.recordId);
    formatasset({inventoryIDList: invid})
    .then(result => {
        this.dispatchEvent(
            new ShowToastEvent({
                title: 'Success',
                message: 'Format Successful',
                variant: 'success',
            }),
        );
        this.isModalOpen1=false;
        eval("$A.get('e.force:refreshView').fire();");
       
    })
    .catch(error => {
        this.error = error;
        alert('eee' + this.error) ;
        this.isModalOpen1=false;
    })
}
    formathandler()
    {
this.isModalOpen1 = true
    }
    handlemodalclose(){
        this.isModalOpen1=false;
    }

    handlehrfclick(event)
    {
        event.preventDefault()
       if(this.isclose==false)
       {
           this.isclose=true;
           
       } 
       else
       {
        this.isclose=false; 
       }
    }
    connectedCallback()
    {

    }

    handlemodal()
    {
        console.log('aqqqq1');
        this.modalval=true;
    }
    closemodal1()
    {
        console.log('aqqqq');
        this.modalval=false;
    }
    childvalues(event)
    {
        this.modalval=false;
        eval("$A.get('e.force:refreshView').fire();");
        // if(event.detail!=null && event.detail!=undefined && event.detail.length>0)
        // {
        //     this.addrecords=event.detail;
            

        // }

    }

}