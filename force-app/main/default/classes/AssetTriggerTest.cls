@isTest
public class AssetTriggerTest {
    @isTest static void createAsset() {
        try {
            
            // Setup test data
            // Create a unique UserName
            String uniqueUserName = 'standarduser' + DateTime.now().getTime() + '@testorg.com';
            // This code runs as the system user
            Profile p = [SELECT Id FROM Profile WHERE Name='Standard User'];
            User u = new User(Alias = 'standt', Email='standarduser@testorg.com',
                              EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US',
                              LocaleSidKey='en_US', ProfileId = p.Id, 
                              TimeZoneSidKey='America/Los_Angeles',
                              UserName=uniqueUserName);
            
            System.runAs(u) {
                Employee__c employee = new Employee__c();
                employee.Name = 'Ranjith Raja';
                employee.AMT_Employee_Type__c = 'Permanent Employee';
                employee.Business_Division__c = 'SALESFORCE';
                employee.Designation__c = 'Salesforce Developer';
                employee.Country__c = 'India';
                employee.Location__c = 'Pune';
                employee.Unique_Employee_No__c = 'S0842';
                employee.AMT_Primary_Email__c = 'poornima@sedintechnologies.com';
                
                insert employee;
                
                Inventory__c hardwareInventory = new Inventory__c();
                hardwareInventory.Product_Family__c = 'Hardware';
                hardwareInventory.Product_Type__c = 'Monitor';
                hardwareInventory.Product_Code__c = 'Monitor - 1';
                hardwareInventory.Division__c = 'SALESFORCE';
                hardwareInventory.Owner__c = 'SEDIN';
                hardwareInventory.Country__c = 'India';
                hardwareInventory.Location__c = 'Pune';
                hardwareInventory.Supplier_Vendor__c = 'Amazon';
                hardwareInventory.License_Count__c = 10;
                
                insert hardwareInventory;
                
                Inventory__c softwareInventory = new Inventory__c();
                softwareInventory.Product_Family__c = 'Software';
                softwareInventory.Product_Code__c = 'MS-Office';
                softwareInventory.Division__c = 'SALESFORCE';
                softwareInventory.Owner__c = 'SEDIN';
                softwareInventory.Country__c = 'India';
                softwareInventory.Location__c = 'Pune';
                softwareInventory.Supplier_Vendor__c = 'Amazon';
                softwareInventory.License_Count__c = 1;
                
                insert softwareInventory;
                
                Asset asst = new Asset();
                asst.Employee_Name__c= employee.Id;
                asst.Product_Name__c = hardwareInventory.Id;
                asst.Inventory__c = hardwareInventory.Id;
                asst.SerialNumber= 'RANDIN0990';
                asst.Brand__c= 'Dell';
                asst.Active__c= true;
                insert asst;
                
                asst.Active__c = false;
                update asst;
                
                Asset ast = new Asset();
                ast.Employee_Name__c= employee.Id;
                ast.Product_Name__c = softwareInventory.Id;
                ast.Inventory__c = softwareInventory.Id;
                ast.SerialNumber= 'RANDIN0990';
                ast.Brand__c= 'Dell';
                ast.Active__c= true;
                insert ast;
                
                ast.Active__c = false;
                update ast;
                
                Asset assetList = [ SELECT Id, Name, Active__c FROM Asset WHERE Id = : ast.Id ];
                System.assertEquals(false, assetList.Active__c);
            }   
        } catch (Exception e) {
            System.debug('Exception -->' +e);
        }
    }
}